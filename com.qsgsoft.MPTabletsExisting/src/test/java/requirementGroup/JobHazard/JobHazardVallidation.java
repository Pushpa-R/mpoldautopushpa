package requirementGroup.JobHazard;

import org.testng.annotations.Test;

import lib.dataObject.FuelData;
import lib.dataObject.JobHazardData;
import lib.dataObject.LoginData;
import lib.page.JobHazardPage;
import lib.page.FuelPage;
import lib.page.HomePage;
import lib.page.LoginPage;
import lib.page.YopMailPage;
import lib.locators.CreateJobHazard;
import lib.locators.CreateJobHazard.JobHazardLocators;
import lib.locators.Yopmail;
import lib.locators.CreateFuel.FuelLocators;
import qaframework.Configuration.Config_MobileAndWeb;

public class JobHazardVallidation extends Config_MobileAndWeb {
	
	public void login() throws Exception  {
		LoginPage objLogin = new LoginPage(IOS_Driver);
		HomePage objHome = new HomePage(IOS_Driver);
		LoginData objLoginData = new LoginData();
		JobHazardPage jobHazard = new JobHazardPage(IOS_Driver);
		objLogin.enterPhoneNumber(objLoginData.ForemanLoggedInNumber).clickSignIn();
		objHome.clickJobHazard();
		jobHazard.clickElementByName(JobHazardLocators.addIcon);		
	}
	
	 public void createJobHazard() throws Exception {
			
			JobHazardPage jobHazard = new JobHazardPage(IOS_Driver);
			JobHazardData objJobHazardData = new JobHazardData();			
			jobHazard.clickElementByName(JobHazardLocators.dateCompleted);						
			jobHazard.enterValueInJobHazard(objJobHazardData.jobNumber, JobHazardLocators.jobNumber);	
			jobHazard.enterValueInJobHazard(objJobHazardData.projectLocation, JobHazardLocators.projectLocation);	
			jobHazard.enterValueInJobHazard(objJobHazardData.wo, JobHazardLocators.wo);					
			jobHazard.enterValueInJobHazard(objJobHazardData.description, JobHazardLocators.description);  			
			jobHazard.enterValueInJobHazard(objJobHazardData.safetyManagerName, JobHazardLocators.safetyManagerName);					
			jobHazard.enterValueInJobHazard(objJobHazardData.safetyManagerPhoneNumber, JobHazardLocators.safetyManagerPhoneNumber);	
			jobHazard.enterValueInJobHazard(objJobHazardData.safetyManagerNotes, JobHazardLocators.safetyManagerNotes);	
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.superVisorName, JobHazardLocators.superVisorName);					
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.superVisorPhoneNumber, JobHazardLocators.superVisorPhoneNumber); 			
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.superVisorNotes, JobHazardLocators.superVisorNotes);					
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.nearestEmsName, JobHazardLocators.nearestEmsName);	
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.nearesrEmsPhoneNumber, JobHazardLocators.nearesrEmsPhoneNumber);	
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.nearestEmsNotes, JobHazardLocators.nearestEmsNotes);		
			jobHazard.clickElementByName(JobHazardLocators.voiceRecordStart);
		    jobHazard.clickElementByName(JobHazardLocators.stopVoiceRecord);
			jobHazard.clickElementByName(JobHazardLocators.firstScreenNext);
			jobHazard.enterValueInJobHazard(objJobHazardData.substation, JobHazardLocators.substation);	
			jobHazard.enterValueInJobHazardFromxpath( objJobHazardData.circuit,JobHazardLocators.circuit);	
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.voltage, JobHazardLocators.voltage);				
			jobHazard.enterValueInJobHazard(objJobHazardData.recloser, JobHazardLocators.recloser);		
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.compassSwitchOrder, JobHazardLocators.compassSwitchOrder);		
			jobHazard.clickElementByName(JobHazardLocators.hotLineNo);
			jobHazard.clickElementByName(JobHazardLocators.clearanceYes);
			jobHazard.enterValueInJobHazard(objJobHazardData.foreman, JobHazardLocators.foreman);		
			jobHazard.enterValueInJobHazard(objJobHazardData.lineman, JobHazardLocators.lineman);	
			jobHazard.clickElementByName(JobHazardLocators.secondScreenNext);
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.apprentice, JobHazardLocators.apprentice);		
			jobHazard.enterValueInJobHazard(objJobHazardData.operator, JobHazardLocators.operator);	
			jobHazard.enterValueInJobHazard(objJobHazardData.customer, JobHazardLocators.customer);		
			jobHazard.enterValueInJobHazard(objJobHazardData.other, JobHazardLocators.other);				
			jobHazard.enterValueInJobHazard(objJobHazardData.specialPrecautions, JobHazardLocators.specialPrecautions);	
			jobHazard.clickElementByName(JobHazardLocators.thirdScreenNext);
			jobHazard.clickElementByxpath(JobHazardLocators.attachButton);
			jobHazard.takePhotos();	 
			jobHazard.okButtonTappedForAlertSubmission(objJobHazardData.OK);
			jobHazard.takePhotosNew(); 	
			jobHazard.clickElementByName(JobHazardLocators.foruthScreenNext);
			jobHazard.clickElementByxpath(JobHazardLocators.workAreaHazard);	
			jobHazard.clickElementByxpath(JobHazardLocators.ConfinedCheck);
			jobHazard.clickElementByxpath(JobHazardLocators.movingCheck);
			jobHazard.clickElementByxpath(JobHazardLocators.workOtherCheck);	
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.workOther, JobHazardLocators.workOther);	
			jobHazard.clickElementByName(JobHazardLocators.workHazardDone);
			jobHazard.clickElementByName(JobHazardLocators.foruthScreenNext);
			jobHazard.clickElementByName(JobHazardLocators.fifthScreenNext);		
			       
	}
	 
	 public void createJobHazardSecond() throws Exception {
			
			JobHazardPage jobHazard = new JobHazardPage(IOS_Driver);
			JobHazardData objJobHazardData = new JobHazardData();			
			jobHazard.clickElementByName(JobHazardLocators.dateCompleted);						
			jobHazard.enterValueInJobHazard(objJobHazardData.jobNumbersecond, JobHazardLocators.jobNumber);	
			jobHazard.enterValueInJobHazard(objJobHazardData.projectLocation, JobHazardLocators.projectLocation);	
			jobHazard.enterValueInJobHazard(objJobHazardData.wo, JobHazardLocators.wo);					
			jobHazard.enterValueInJobHazard(objJobHazardData.description, JobHazardLocators.description);  			
			jobHazard.enterValueInJobHazard(objJobHazardData.safetyManagerName, JobHazardLocators.safetyManagerName);					
			jobHazard.enterValueInJobHazard(objJobHazardData.safetyManagerPhoneNumber, JobHazardLocators.safetyManagerPhoneNumber);	
			jobHazard.enterValueInJobHazard(objJobHazardData.safetyManagerNotes, JobHazardLocators.safetyManagerNotes);	
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.superVisorName, JobHazardLocators.superVisorName);					
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.superVisorPhoneNumber, JobHazardLocators.superVisorPhoneNumber); 			
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.superVisorNotes, JobHazardLocators.superVisorNotes);					
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.nearestEmsName, JobHazardLocators.nearestEmsName);	
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.nearesrEmsPhoneNumber, JobHazardLocators.nearesrEmsPhoneNumber);	
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.nearestEmsNotes, JobHazardLocators.nearestEmsNotes);		
			jobHazard.clickElementByName(JobHazardLocators.voiceRecordStart);
		    jobHazard.clickElementByName(JobHazardLocators.stopVoiceRecord);
			jobHazard.clickElementByName(JobHazardLocators.firstScreenNext);
			jobHazard.enterValueInJobHazard(objJobHazardData.substation, JobHazardLocators.substation);	
			jobHazard.enterValueInJobHazardFromxpath( objJobHazardData.circuit,JobHazardLocators.circuit);	
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.voltage, JobHazardLocators.voltage);				
			jobHazard.enterValueInJobHazard(objJobHazardData.recloser, JobHazardLocators.recloser);		
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.compassSwitchOrder, JobHazardLocators.compassSwitchOrder);		
			jobHazard.clickElementByName(JobHazardLocators.hotLineNo);
			jobHazard.clickElementByName(JobHazardLocators.clearanceYes);
			jobHazard.enterValueInJobHazard(objJobHazardData.foreman, JobHazardLocators.foreman);		
			jobHazard.enterValueInJobHazard(objJobHazardData.lineman, JobHazardLocators.lineman);	
			jobHazard.clickElementByName(JobHazardLocators.secondScreenNext);
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.apprentice, JobHazardLocators.apprentice);		
			jobHazard.enterValueInJobHazard(objJobHazardData.operator, JobHazardLocators.operator);	
			jobHazard.enterValueInJobHazard(objJobHazardData.customer, JobHazardLocators.customer);		
			jobHazard.enterValueInJobHazard(objJobHazardData.other, JobHazardLocators.other);				
			jobHazard.enterValueInJobHazard(objJobHazardData.specialPrecautions, JobHazardLocators.specialPrecautions);	
			jobHazard.clickElementByName(JobHazardLocators.thirdScreenNext);
			jobHazard.clickElementByxpath(JobHazardLocators.attachButton);
			jobHazard.takePhotos();	 
			jobHazard.okButtonTappedForAlertSubmission(objJobHazardData.OK);
			jobHazard.takePhotosNew(); 	
			jobHazard.clickElementByName(JobHazardLocators.foruthScreenNext);
			jobHazard.clickElementByxpath(JobHazardLocators.workAreaHazard);	
			jobHazard.clickElementByxpath(JobHazardLocators.ConfinedCheck);
			jobHazard.clickElementByxpath(JobHazardLocators.movingCheck);
			jobHazard.clickElementByxpath(JobHazardLocators.workOtherCheck);	
			jobHazard.enterValueInJobHazardFromxpath(objJobHazardData.workOther, JobHazardLocators.workOther);	
			jobHazard.clickElementByName(JobHazardLocators.workHazardDone);
			jobHazard.clickElementByName(JobHazardLocators.foruthScreenNext);
			jobHazard.clickElementByName(JobHazardLocators.fifthScreenNext);		
			       
	}
	 
	 /**********************************************************************************************************
		 * 'Description: Verify that able to create the report with attachement and submit
		 *  'Date: 30-4-2018
		 * 'Author: Ashwini Acharya
		 **********************************************************************************************************/
		
		@Test(priority = 7, description = "Verify that able to create the report by attaching pictures from Gallery and submit")
	    public void RTS1() throws Exception {

			TCID = "RTS1";
			strTO = "Verify that able to create the report by attaching pictures from Gallery and submit";
			JobHazardPage jobHazard = new JobHazardPage(IOS_Driver);
			JobHazardData objjobHazardData = new JobHazardData();
			login();
			createJobHazard();			
			jobHazard.clickSubmit(JobHazardLocators.submitButton);
			jobHazard.verReportIsSubmitted(objjobHazardData.alertForSubmit);
			jobHazard.okButtonTappedForAlertSubmission(objjobHazardData.OK);		
		}
	 
		/**********************************************************************************************************
		 * 'Description: Verify that able to create the report with attachement and submit
		 *  'Date: 02-May-2018
		 * 'Author: Ashwini Acharya
		 **********************************************************************************************************/
		
		@Test(priority = 8, description = "Verify that able to create the report with attachement and submit and receive the email")
	    public void RTS2() throws Exception {

			TCID = "RTS2";
			strTO = "Verify that able to create the report with attachement and submit and receive the email";
			JobHazardPage jobHazard = new JobHazardPage(IOS_Driver);
			JobHazardData objjobHazardData = new JobHazardData();
			YopMailPage objYopMail = new YopMailPage(this.wdriver);
			jobHazard.clickElementByName(JobHazardLocators.addIcon);		
			createJobHazardSecond();			
			jobHazard.clickSubmit(JobHazardLocators.submitButton);
			jobHazard.verReportIsSubmitted(objjobHazardData.alertForSubmit);
			jobHazard.okButtonTappedForAlertSubmission(objjobHazardData.OK);
		    // launch yopmail
		    objYopMail.launchYopmail().verifyEmailIdField().enterAgentEmailAddressInYopmail("pushpa@yopmail.com").verifyCheckInboxButton().clickCheckInboxButton().clickCheckForNewMailsButton().verifySubject();
		    gstrResult = "PASS";
			
		}
   
}
