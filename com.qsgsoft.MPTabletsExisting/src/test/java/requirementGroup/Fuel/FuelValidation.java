package requirementGroup.Fuel;

import org.testng.annotations.Test;

import lib.dataObject.FuelData;
import lib.dataObject.LoginData;
import lib.page.FuelPage;
import lib.page.HomePage;
import lib.page.LoginPage;
import lib.page.YopMailPage;
import lib.locators.CreateFuel;
import lib.locators.CreateFuel.FuelLocators;
import lib.locators.Yopmail;
import qaframework.Configuration.Config_MobileAndWeb;

public class FuelValidation extends Config_MobileAndWeb {
	
	public void login() throws Exception {
		LoginPage objLogin = new LoginPage(IOS_Driver);
		HomePage objHome = new HomePage(IOS_Driver);
		LoginData objLoginData = new LoginData();
		FuelPage fuel = new FuelPage(IOS_Driver);
		objLogin.enterPhoneNumber(objLoginData.MechanicLoggedInNumber).clickSignIn();
		objHome.clickFuel();
		fuel.clickElementByName(FuelLocators.addIcon);		
	}
	
    public void createFuel() throws Exception {
		
		FuelPage fuel = new FuelPage(IOS_Driver);
		FuelData objFuelData = new FuelData();						
		fuel.enterValueInFuel(objFuelData.jobNumber, FuelLocators.jobNumber);		
		fuel.clickElementByxpath( FuelLocators.receiptDate);	
		//fuel.selectDateByScrollingDate( FuelLocators.receiptDate);	
		fuel.enterValueInFuel(objFuelData.vehicleNumber, FuelLocators.vehicleNumber);	
		fuel.enterValueInFuel(objFuelData.mileage, FuelLocators.mileage);	
		fuel.enterValueInFuel(objFuelData.offLoadGallon, FuelLocators.offLoadGallon);					
        fuel.enterValueInFuel(objFuelData.remarks, FuelLocators.remarks);        
	}
    
     public void createFuelSecond() throws Exception {
		
		FuelPage fuel = new FuelPage(IOS_Driver);
		FuelData objFuelData = new FuelData();						
		fuel.enterValueInFuel(objFuelData.jobNumbersecond, FuelLocators.jobNumber);		
		fuel.clickElementByxpath( FuelLocators.receiptDate);	
		//fuel.selectDateByScrollingDate( FuelLocators.receiptDate);	
		fuel.enterValueInFuel(objFuelData.vehicleNumber, FuelLocators.vehicleNumber);	
		fuel.enterValueInFuel(objFuelData.mileage, FuelLocators.mileage);	
		fuel.enterValueInFuel(objFuelData.offLoadGallon, FuelLocators.offLoadGallon);					
        fuel.enterValueInFuel(objFuelData.remarks, FuelLocators.remarks);        
	}
    
	/**********************************************************************************************************
	 * 'Description: Verify that able to create the report with attachement and submit
	 *  'Date: 25-4-2018
	 * 'Author: Ashwini Acharya
	 **********************************************************************************************************/
	
	@Test(priority = 3, description = "Verify that able to create the report by attaching pictures from Camera and submit")
    public void RTS1() throws Exception {

		TCID = "RTS1";
		strTO = "Verify that able to create the report by attaching pictures from Camera and submit";
		FuelPage fuel = new FuelPage(IOS_Driver);
		FuelData objfuelData = new FuelData();
		login();
		createFuel();
		fuel.takePhotos();	 
		fuel.okButtonTappedForAlertSubmission(objfuelData.OK);
		fuel.takePhotosNew(); 
		fuel.clickSave();
		fuel.clickSubmit(FuelLocators.submit);
		fuel.verReportIsSubmitted(objfuelData.alertForSubmittedReport);
		fuel.okButtonTappedForAlertSubmission(objfuelData.OK);		
	}
	
	/**********************************************************************************************************
	 * 'Description: Verify that able to create the report with attachement and submit
	 *  'Date: 25-4-2018
	 * 'Author: Ashwini Acharya
	 **********************************************************************************************************/
	
	@Test(priority = 4, description = "Verify that able to create the report with attachement and submit")
    public void RTS2() throws Exception {

		TCID = "RTS2";
		strTO = "Verify that able to create the report with attachement and submit";
		FuelPage fuel = new FuelPage(IOS_Driver);
		FuelData objFuelData = new FuelData();
		YopMailPage objYopMail = new YopMailPage(this.wdriver);
		fuel.clickElementByName(FuelLocators.addIcon);		
		createFuelSecond();
		fuel.takePhotos();	 
		fuel.okButtonTappedForAlertSubmission(objFuelData.OK);
		fuel.takePhotosNew();  
		fuel.clickSave();
		fuel.clickSubmit(FuelLocators.submit);
		fuel.verReportIsSubmitted(objFuelData.alertForSubmittedReport);
		fuel.okButtonTappedForAlertSubmission(objFuelData.OK);
	    // launch yopmail
	    objYopMail.launchYopmail().verifyEmailIdField().enterAgentEmailAddressInYopmail("pushpa@yopmail.com").verifyCheckInboxButton().clickCheckInboxButton().clickCheckForNewMailsButton().verifySubject();
	    
	    gstrResult = "PASS";
		
	}
	
}
