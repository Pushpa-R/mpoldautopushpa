package requirementGroup.MonthlyBluesheet;
import org.testng.annotations.Test;

import lib.dataObject.MonthlyBluesheetData;
import lib.dataObject.IncidentData;
import lib.dataObject.LoginData;
import lib.locators.CreateExpense.Locators;
import lib.locators.CreateFuel.FuelLocators;
import lib.locators.CreateIncident.IncidentLocators;
import lib.locators.CreateMonthlyBluesheet.MonthlyBluesheetLocators;
import lib.locators.CreateProjectSite.ProjectSiteLocators;
import lib.page.MonthlyBluesheetPage;
import lib.page.YopMailPage;
import lib.page.FuelPage;
import lib.page.HomePage;
import lib.page.IncidentPage;
import lib.page.LoginPage;
import qaframework.Configuration.Config_MobileAndWeb;

public class MonthlyBluesheetValidation  extends Config_MobileAndWeb {
	
	public void login() throws Exception {
		LoginPage objLogin = new LoginPage(IOS_Driver);
		HomePage objHome = new HomePage(IOS_Driver);
		LoginData objLoginData = new LoginData();
		MonthlyBluesheetPage monthlyBluesheet = new MonthlyBluesheetPage(IOS_Driver);
		objLogin.enterPhoneNumber(objLoginData.ForemanLoggedInNumber).clickSignIn();		
		objHome.clickMonthlyBluesheet();
		monthlyBluesheet.okMonthlyBluesheetAlert(MonthlyBluesheetLocators.alertOkButton);
		monthlyBluesheet.clickElementByName(MonthlyBluesheetLocators.addIcon);	
	}
	
	public void createMonthlyBluesheet() throws Exception {
		MonthlyBluesheetPage monthlyBluesheet = new MonthlyBluesheetPage(IOS_Driver);
		MonthlyBluesheetData objMonthlyBluesheetData = new MonthlyBluesheetData();						
		monthlyBluesheet.clickElementByName(MonthlyBluesheetLocators.date);				
		monthlyBluesheet.enterValueInMonthlyBluesheet(objMonthlyBluesheetData.unit, MonthlyBluesheetLocators.unit);	
		monthlyBluesheet.enterValueInMonthlyBluesheet(objMonthlyBluesheetData.odometer, MonthlyBluesheetLocators.odometer);
		monthlyBluesheet.enterValueInMonthlyBluesheet(objMonthlyBluesheetData.job, MonthlyBluesheetLocators.job);
		monthlyBluesheet.enterValueInMonthlyBluesheetFromxpath(objMonthlyBluesheetData.inspectedBy, MonthlyBluesheetLocators.inspectedBy);
		monthlyBluesheet.clickElementByxpath(MonthlyBluesheetLocators.firstAidKit);
		monthlyBluesheet.clickElementByName(MonthlyBluesheetLocators.firstAidRadioYes);
		monthlyBluesheet.enterValueInMonthlyBluesheet(objMonthlyBluesheetData.fullComment, MonthlyBluesheetLocators.fullComment);	
		monthlyBluesheet.clickElementByxpath(MonthlyBluesheetLocators.triangles);
		monthlyBluesheet.clickElementByName(MonthlyBluesheetLocators.trianglesGoodNo);
		monthlyBluesheet.enterValueInMonthlyBluesheetFromxpath(objMonthlyBluesheetData.trianglesGoodComment, MonthlyBluesheetLocators.trianglesGoodComment);	
		monthlyBluesheet.clickElementByName(MonthlyBluesheetLocators.needMoreRadio);
		monthlyBluesheet.enterValueInMonthlyBluesheetFromxpath(objMonthlyBluesheetData.needMoreComment, MonthlyBluesheetLocators.needMoreComment);	
		monthlyBluesheet.enterValueInMonthlyBluesheet(objMonthlyBluesheetData.miscComment, MonthlyBluesheetLocators.miscComment);	
		monthlyBluesheet.scrollTableView(MonthlyBluesheetLocators.scrollView);
		monthlyBluesheet.clickElementByName(MonthlyBluesheetLocators.attachPhoto);
		monthlyBluesheet.takePhotos();	 
		monthlyBluesheet.okButtonTappedForAlertSubmission(objMonthlyBluesheetData.OK);
		monthlyBluesheet.takePhotosNew(); 
		monthlyBluesheet.clickElementByName(MonthlyBluesheetLocators.saveButton);						
	}
	
	public void createMonthlyBluesheetSecond() throws Exception {
		MonthlyBluesheetPage monthlyBluesheet = new MonthlyBluesheetPage(IOS_Driver);
		MonthlyBluesheetData objMonthlyBluesheetData = new MonthlyBluesheetData();				
		monthlyBluesheet.clickElementByName(MonthlyBluesheetLocators.date);				
		monthlyBluesheet.enterValueInMonthlyBluesheet(objMonthlyBluesheetData.unitSecond, MonthlyBluesheetLocators.unit);	
		monthlyBluesheet.enterValueInMonthlyBluesheet(objMonthlyBluesheetData.odometer, MonthlyBluesheetLocators.odometer);
		monthlyBluesheet.enterValueInMonthlyBluesheet(objMonthlyBluesheetData.job, MonthlyBluesheetLocators.job);
		monthlyBluesheet.enterValueInMonthlyBluesheetFromxpath(objMonthlyBluesheetData.inspectedBy, MonthlyBluesheetLocators.inspectedBy);
		monthlyBluesheet.clickElementByxpath(MonthlyBluesheetLocators.firstAidKit);
		monthlyBluesheet.clickElementByName(MonthlyBluesheetLocators.firstAidRadioYes);
		monthlyBluesheet.enterValueInMonthlyBluesheet(objMonthlyBluesheetData.fullComment, MonthlyBluesheetLocators.fullComment);	
		monthlyBluesheet.clickElementByxpath(MonthlyBluesheetLocators.triangles);
		monthlyBluesheet.clickElementByxpath(MonthlyBluesheetLocators.trianglesGoodNo);
		monthlyBluesheet.enterValueInMonthlyBluesheetFromxpath(objMonthlyBluesheetData.trianglesGoodComment, MonthlyBluesheetLocators.trianglesGoodComment);	
		monthlyBluesheet.clickElementByName(MonthlyBluesheetLocators.needMoreRadio);
		monthlyBluesheet.enterValueInMonthlyBluesheetFromxpath(objMonthlyBluesheetData.needMoreComment, MonthlyBluesheetLocators.needMoreComment);	
		monthlyBluesheet.enterValueInMonthlyBluesheet(objMonthlyBluesheetData.miscComment, MonthlyBluesheetLocators.miscComment);	
		monthlyBluesheet.scrollTableView(MonthlyBluesheetLocators.scrollView);
		monthlyBluesheet.clickElementByName(MonthlyBluesheetLocators.attachPhoto);
		monthlyBluesheet.takePhotos();	 
		monthlyBluesheet.okButtonTappedForAlertSubmission(objMonthlyBluesheetData.OK);
		monthlyBluesheet.takePhotosNew(); 
		monthlyBluesheet.clickElementByName(MonthlyBluesheetLocators.saveButton);						
	}
	
	 /**********************************************************************************************************
	 * 'Description: Verify that able to create the report with attachement and submit
	 *  'Date: 10-May-2018
	 * 'Author: Ashwini Acharya
	 **********************************************************************************************************/
	
	@Test(priority = 1, description = "Verify that able to create the report by attaching pictures from Gallery and submit")
    public void RTS1() throws Exception {

		TCID = "RTS1";
		strTO = "Verify that able to create the report by attaching pictures from Gallery and submit";
				
		MonthlyBluesheetPage monthlyBluesheet = new MonthlyBluesheetPage(IOS_Driver);
		MonthlyBluesheetData objmonthlyBluesheetData = new MonthlyBluesheetData();		
		login();			
		createMonthlyBluesheet();			
		monthlyBluesheet.clickSubmit(MonthlyBluesheetLocators.submit);				
		monthlyBluesheet.verReportIsSubmitted(objmonthlyBluesheetData.alertForSubmit);
		monthlyBluesheet.okButtonTappedForAlertSubmission(objmonthlyBluesheetData.OK);	
	}
	
	/**********************************************************************************************************
	 * 'Description: Verify that able to create the report with attachement and submit
	 *  'Date: 10-May-2018
	 * 'Author: Ashwini Acharya
	 **********************************************************************************************************/
	
	@Test(priority = 2, description = "Verify that able to create the report with attachement and submit and receive the email")
    public void RTS2() throws Exception {

		TCID = "RTS2";
		strTO = "Verify that able to create the report with attachement and submit and receive the email";
		MonthlyBluesheetPage monthlyBluesheet = new MonthlyBluesheetPage(IOS_Driver);
		MonthlyBluesheetData objmonthlyBluesheetData = new MonthlyBluesheetData();		
		YopMailPage objYopMail = new YopMailPage(this.wdriver);
		monthlyBluesheet.clickElementByName(MonthlyBluesheetLocators.addIcon);		
		createMonthlyBluesheetSecond();			
		monthlyBluesheet.clickSubmit(IncidentLocators.submitButton);
		monthlyBluesheet.clickSubmit(IncidentLocators.doneButton);
		monthlyBluesheet.verReportIsSubmitted(objmonthlyBluesheetData.alertForSubmit);
		monthlyBluesheet.okButtonTappedForAlertSubmission(objmonthlyBluesheetData.OK);
	    // launch yopmail
	    objYopMail.launchYopmail().verifyEmailIdField().enterAgentEmailAddressInYopmail("pushpa@yopmail.com").verifyCheckInboxButton().clickCheckInboxButton().clickCheckForNewMailsButton().verifySubject();
	    gstrResult = "PASS";
		
	}

}
