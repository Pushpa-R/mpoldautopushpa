package requirementGroup.Incident;

import org.testng.annotations.Test;

import lib.dataObject.IncidentData;
import lib.dataObject.JobHazardData;
import lib.dataObject.LoginData;
import lib.page.IncidentPage;
import lib.page.JobHazardPage;
import lib.page.HomePage;
import lib.page.LoginPage;
import lib.page.YopMailPage;
import lib.locators.CreateIncident;
import lib.locators.CreateIncident.IncidentLocators;
import lib.locators.CreateJobHazard.JobHazardLocators;
import lib.locators.Yopmail;
import qaframework.Configuration.Config_MobileAndWeb;

public class IncidentValidation extends Config_MobileAndWeb {
	
	public void login() throws Exception {
		LoginPage objLogin = new LoginPage(IOS_Driver);
		HomePage objHome = new HomePage(IOS_Driver);
		LoginData objLoginData = new LoginData();
		IncidentPage incident = new IncidentPage(IOS_Driver);
		objLogin.enterPhoneNumber(objLoginData.MechanicLoggedInNumber).clickSignIn();
		objHome.clickIncident();
		incident.clickElementByName(IncidentLocators.addIcon);		
	}
	
	 public void createIncident() throws Exception {
			
			IncidentPage incident = new IncidentPage(IOS_Driver);
			IncidentData objIncident = new IncidentData();
				
			incident.clickElementByName(IncidentLocators.nearmiss);		
			incident.clickElementByName(IncidentLocators.vehicle);				
			incident.enterValueInIncident(objIncident.empName, IncidentLocators.EmpName);
			incident.clickElementByName(IncidentLocators.dateOfIncident);	
			incident.clickElementByName(IncidentLocators.dateOfEvaluation);	
			incident.clickElementByName(IncidentLocators.time);
			//incident.selecTimeByScrolling(IncidentLocators.scrollingTime);	
			incident.clickElementByName(IncidentLocators.toolbarDone);
			incident.enterValueInIncident(objIncident.jobNumber, IncidentLocators.jobNumber);
			incident.enterValueInIncident(objIncident.addressOfIncident, IncidentLocators.addressOfIncident);
			incident.enterValueInIncident(objIncident.ExplainIncident, IncidentLocators.ExplainIncident);
			incident.clickElementByName(IncidentLocators.firstNext);
			incident.clickElementByName(IncidentLocators.handTools);	
			incident.clickElementByName(IncidentLocators.PowerTools);	
			incident.clickElementByName(IncidentLocators.trainingYes);			
			incident.enterValueInIncident(objIncident.trainingText, IncidentLocators.trainingText);	
			incident.clickElementByName(IncidentLocators.avoidYes);			
			incident.enterValueInIncident(objIncident.avoidText, IncidentLocators.avoidText);	
			incident.clickElementByName(IncidentLocators.secondNext);
			incident.clickElementByxpath(IncidentLocators.witnessYes);
			incident.enterValueInIncidentFromxpath(objIncident.witnessText, IncidentLocators.witnessText);	
			incident.clickElementByName(IncidentLocators.faulty);
			incident.clickElementByName(IncidentLocators.poor);
			incident.enterValueInIncident(objIncident.explainText, IncidentLocators.explainText);
			incident.clickElementByName(IncidentLocators.thirdNext);
			incident.enterValueInIncident(objIncident.unit, IncidentLocators.unit);
			incident.enterValueInIncident(objIncident.ticket, IncidentLocators.ticket);	
		    incident.clickElementByName(IncidentLocators.attachPhoto);
		    incident.takePhotos();	 
		    incident.okButtonTappedForAlertSubmission(objIncident.OK);
		    incident.takePhotosNew(); 
			incident.clickElementByName(IncidentLocators.fourthNext);			
			incident.enterSignature(IncidentLocators.employeeSig);
			incident.enterValueInIncident(objIncident.employeeName, IncidentLocators.employeeName);
			incident.clickElementByName(IncidentLocators.employeeDate);		
			
			//incident.enterSignature(IncidentLocators.supervisorSig);
			//incident.enterValueInIncident(objIncident.supervisorName, IncidentLocators.supervisorName);
			//incident.clickElementByName(IncidentLocators.supervisorDate);
			incident.clickElementByName(IncidentLocators.saveButton);				
	}
	 
	 public void createIncidentSecond() throws Exception {
			
			IncidentPage incident = new IncidentPage(IOS_Driver);
			IncidentData objIncident = new IncidentData();
				
			incident.clickElementByName(IncidentLocators.nearmiss);		
			incident.clickElementByName(IncidentLocators.vehicle);				
			incident.enterValueInIncident(objIncident.empNameSecond, IncidentLocators.EmpName);
			incident.clickElementByName(IncidentLocators.dateOfIncident);	
			incident.clickElementByName(IncidentLocators.dateOfEvaluation);	
			incident.clickElementByName(IncidentLocators.time);
			//incident.selecTimeByScrolling(IncidentLocators.scrollingTime);	
			incident.clickElementByName(IncidentLocators.toolbarDone);
			incident.enterValueInIncident(objIncident.jobNumber, IncidentLocators.jobNumber);
			incident.enterValueInIncident(objIncident.addressOfIncident, IncidentLocators.addressOfIncident);
			incident.enterValueInIncident(objIncident.ExplainIncident, IncidentLocators.ExplainIncident);
			incident.clickElementByName(IncidentLocators.firstNext);
			incident.clickElementByName(IncidentLocators.handTools);	
			incident.clickElementByName(IncidentLocators.PowerTools);	
			incident.clickElementByName(IncidentLocators.trainingYes);			
			incident.enterValueInIncident(objIncident.trainingText, IncidentLocators.trainingText);	
			incident.clickElementByName(IncidentLocators.avoidYes);			
			incident.enterValueInIncident(objIncident.avoidText, IncidentLocators.avoidText);	
			incident.clickElementByName(IncidentLocators.secondNext);
			incident.clickElementByxpath(IncidentLocators.witnessYes);
			incident.enterValueInIncidentFromxpath(objIncident.witnessText, IncidentLocators.witnessText);	
			incident.clickElementByName(IncidentLocators.faulty);
			incident.clickElementByName(IncidentLocators.poor);
			incident.enterValueInIncident(objIncident.explainText, IncidentLocators.explainText);
			incident.clickElementByName(IncidentLocators.thirdNext);
			incident.enterValueInIncident(objIncident.unit, IncidentLocators.unit);
			incident.enterValueInIncident(objIncident.ticket, IncidentLocators.ticket);	
		    incident.clickElementByName(IncidentLocators.attachPhoto);
		    incident.takePhotos();	 
		    incident.okButtonTappedForAlertSubmission(objIncident.OK);
		    incident.takePhotosNew(); 
			incident.clickElementByName(IncidentLocators.fourthNext);			
			incident.enterSignature(IncidentLocators.employeeSig);
			incident.enterValueInIncident(objIncident.employeeName, IncidentLocators.employeeName);
			incident.clickElementByName(IncidentLocators.employeeDate);		
			
			//incident.enterSignature(IncidentLocators.supervisorSig);
			//incident.enterValueInIncident(objIncident.supervisorName, IncidentLocators.supervisorName);
			//incident.clickElementByName(IncidentLocators.supervisorDate);
			incident.clickElementByName(IncidentLocators.saveButton);				
	}
	 
	 /**********************************************************************************************************
		 * 'Description: Verify that able to create the report with attachement and submit
		 *  'Date: 04-May-2018
		 * 'Author: Ashwini Acharya
		 **********************************************************************************************************/
		
		@Test(priority = 5, description = "Verify that able to create the report by attaching pictures from Gallery and submit")
	    public void RTS1() throws Exception {

			TCID = "RTS1";
			strTO = "Verify that able to create the report by attaching pictures from Gallery and submit";
			IncidentPage incident = new IncidentPage(IOS_Driver);
			IncidentData objincidentData = new IncidentData();
			login();			
			createIncident();			
			incident.clickSubmit(IncidentLocators.submitButton);
			incident.clickSubmit(IncidentLocators.doneButton);
			incident.verReportIsSubmitted(objincidentData.alertForSubmit);
			incident.okButtonTappedForAlertSubmission(objincidentData.OK);		
		}
		
		/**********************************************************************************************************
		 * 'Description: Verify that able to create the report with attachement and submit
		 *  'Date: 02-May-2018
		 * 'Author: Ashwini Acharya
		 **********************************************************************************************************/
		
		@Test(priority = 6, description = "Verify that able to create the report with attachement and submit and receive the email")
	    public void RTS2() throws Exception {

			TCID = "RTS2";
			strTO = "Verify that able to create the report with attachement and submit and receive the email";
			IncidentPage incident = new IncidentPage(IOS_Driver);
			IncidentData objincidentData = new IncidentData();
			YopMailPage objYopMail = new YopMailPage(this.wdriver);
			incident.clickElementByName(IncidentLocators.addIcon);		
			createIncidentSecond();			
			incident.clickSubmit(IncidentLocators.submitButton);
			incident.clickSubmit(IncidentLocators.doneButton);
			incident.verReportIsSubmitted(objincidentData.alertForSubmit);
			incident.okButtonTappedForAlertSubmission(objincidentData.OK);
		    // launch yopmail
		    objYopMail.launchYopmail().verifyEmailIdField().enterAgentEmailAddressInYopmail("pushpa@yopmail.com").verifyCheckInboxButton().clickCheckInboxButton().clickCheckForNewMailsButton().verifySubject();
		    gstrResult = "PASS";
			
		}

}
