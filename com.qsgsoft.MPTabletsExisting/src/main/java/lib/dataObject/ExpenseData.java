package lib.dataObject;

import java.util.Random;

import lib.dataObject.ExpenseData.ConsumerTag;
import qaframework.lib.UserDefinedFunction.ReadDataFromXMLFile;

public class ExpenseData {
	
	public ExpenseData() throws Exception {

	}


	ReadDataFromXMLFile xml = new ReadDataFromXMLFile();
	static final String FILEPATH = "ExpenseDataXml";

	public final String JobNumber = xml.read(ConsumerTag.JobNumber, FILEPATH),
			                         jobNumbersecond = xml.read(ConsumerTag.jobNumbersecond, FILEPATH),
			                         EmployeeName = xml.read(ConsumerTag.EmployeeName, FILEPATH),
			                        	EmployeeNameSecond = xml.read(ConsumerTag.EmployeeNameSecond, FILEPATH),
			                        	VehicleNumber = xml.read(ConsumerTag.VehicleNumber, FILEPATH),
			                        	TotalAmount = xml.read(ConsumerTag.TotalAmount, FILEPATH),
			                        	Description = xml.read(ConsumerTag.Description, FILEPATH),
			                        	AlertForSubmit = xml.read(ConsumerTag.AlertForSubmit, FILEPATH),
			                        	cameraRoll = xml.read(ConsumerTag.cameraRoll, FILEPATH),
			                         photoNameiOS11 = xml.read(ConsumerTag.photoNameiOS11, FILEPATH),
			                         photoNameiOS10 = xml.read(ConsumerTag.photoNameiOS10, FILEPATH),
	                                 OK = xml.read(ConsumerTag.OK, FILEPATH);
												
																									
	public static class ConsumerTag {

		public static final String JobNumber = "JobNumber",
				                                    jobNumbersecond = "jobNumbersecond",
				                                    EmployeeName = "EmployeeName",
				                                   EmployeeNameSecond = "EmployeeNameSecond",
				                                	  VehicleNumber = "VehicleNumber",
				                                	  TotalAmount = "TotalAmount",
				                                   Description = "Description",
				                                   OK = "OK",
				                                   cameraRoll = "cameraRoll",
				                                	  photoNameiOS11 = "photoNameiOS11",
				                                	  photoNameiOS10 = "photoNameiOS10",
				                                   AlertForSubmit = "AlertForSubmit";
		
	}


}
