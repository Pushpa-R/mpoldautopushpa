package lib.dataObject;
import java.util.Random;

import lib.dataObject.ExpenseData.ConsumerTag;
import qaframework.lib.UserDefinedFunction.ReadDataFromXMLFile;

public class FuelData {
	public  FuelData() throws Exception {
		
	}
	ReadDataFromXMLFile xml = new ReadDataFromXMLFile();
	static final String FILEPATH = "FuelDataXml";

	public final String jobNumber = xml.read(ConsumerTag.jobNumber, FILEPATH),
			                         jobNumbersecond = xml.read(ConsumerTag.jobNumbersecond, FILEPATH),
			                         receiptDate = xml.read(ConsumerTag.receiptDate, FILEPATH),
			                         vehicleNumber = xml.read(ConsumerTag.vehicleNumber, FILEPATH),
			                        	mileage = xml.read(ConsumerTag.mileage, FILEPATH),
			                        	offLoadGallon = xml.read(ConsumerTag.offLoadGallon, FILEPATH),
			                        	remarks = xml.read(ConsumerTag.remarks, FILEPATH),
			                        	alertForSubmittedReport = xml.read(ConsumerTag.alertForSubmittedReport, FILEPATH),
			                        	alertforPhotos = xml.read(ConsumerTag.alertforPhotos, FILEPATH),
			                        	cameraRoll = xml.read(ConsumerTag.cameraRoll, FILEPATH),
			                        	photoNameiOS11 = xml.read(ConsumerTag.photoNameiOS11, FILEPATH),
			                         photoNameiOS10 = xml.read(ConsumerTag.photoNameiOS10, FILEPATH),
	                                 OK = xml.read(ConsumerTag.OK, FILEPATH);
			
	public static class ConsumerTag {
		public static final String  jobNumber = "jobNumber",
				                                     jobNumbersecond = "jobNumbersecond",
				                                     receiptDate = "receiptDate",
				                                     vehicleNumber = "vehicleNumber",
				                                     mileage = "mileage",
				                                     offLoadGallon = "offLoadGallon",
				                                     remarks = "remarks",
				                                     alertForSubmittedReport = "alertForSubmittedReport",
				                                    	alertforPhotos = "alertforPhotos",
				                                    	cameraRoll = "cameraRoll",
				                                    	photoName = "photoName",
				                                    	photoNameiOS11 = "photoNameiOS11",
				                                    	photoNameiOS10 = "photoNameiOS10",
				                                    	OK = "OK";
	}
}

