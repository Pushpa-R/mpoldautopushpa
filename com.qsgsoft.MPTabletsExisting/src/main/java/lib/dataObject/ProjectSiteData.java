package lib.dataObject;
import java.util.Random;
import lib.dataObject.ProjectSiteData.ConsumerTag;
import qaframework.lib.UserDefinedFunction.ReadDataFromXMLFile;

public class ProjectSiteData {
	public ProjectSiteData() throws Exception {
		
	}
	ReadDataFromXMLFile xml = new ReadDataFromXMLFile();
	static final String FILEPATH = "ProjectSiteXml";
	
	public final String foreman = xml.read(ConsumerTag.foreman, FILEPATH),
			                         jobNumber = xml.read(ConsumerTag.jobNumber, FILEPATH),
			                         jobNumberSecond = xml.read(ConsumerTag.jobNumberSecond, FILEPATH),
			                         crewLocation = xml.read(ConsumerTag.crewLocation, FILEPATH),
			                        	siteInspection = xml.read(ConsumerTag.siteInspection, FILEPATH),
			                        	correctionText = xml.read(ConsumerTag.correctionText, FILEPATH),
			                        	workPracticeText = xml.read(ConsumerTag.workPracticeText, FILEPATH),
			                        	unsafeWorkText = xml.read(ConsumerTag.unsafeWorkText, FILEPATH),
			                        	toolsText = xml.read(ConsumerTag.toolsText, FILEPATH),
			                        	ppeText = xml.read(ConsumerTag.ppeText, FILEPATH),			                       
			                        	inspectedName = xml.read(ConsumerTag.inspectedName, FILEPATH), 
			                        	photoNameiOS11 = xml.read(ConsumerTag.photoNameiOS11, FILEPATH),
		   							photoNameiOS10 = xml.read(ConsumerTag.photoNameiOS10, FILEPATH),	
			                        	alertForSubmit = xml.read(ConsumerTag.alertForSubmit, FILEPATH),								    	 
								    	OK = xml.read(ConsumerTag.OK, FILEPATH);	
	
	public static class ConsumerTag {
		public static final String foreman = "foreman",
				jobNumber = "jobNumber",
				jobNumberSecond = "jobNumberSecond",
				crewLocation = "crewLocation",
				siteInspection = "siteInspection",
				correctionText = "correctionText",
				workPracticeText = "workPracticeText",
				unsafeWorkText = "unsafeWorkText",
				toolsText = "toolsText",
				ppeText = "ppeText",
				inspectedName = "inspectedName",
				photoNameiOS11 = "photoNameiOS11",
				photoNameiOS10 = "photoNameiOS10",
				alertForSubmit = "alertForSubmit",
				OK = "OK";				
	}
}
