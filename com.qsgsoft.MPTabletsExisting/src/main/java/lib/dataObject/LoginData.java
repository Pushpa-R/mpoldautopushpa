package lib.dataObject;


import qaframework.lib.UserDefinedFunction.ReadDataFromXMLFile;

public class LoginData {
	
	public LoginData() throws Exception {

	}


	ReadDataFromXMLFile xml = new ReadDataFromXMLFile();
	static final String FILEPATH = "LoginDataXml";

	public final String MechanicLoggedInNumber = xml.read(ConsumerTag.MechanicLoggedInNumber, FILEPATH),
			                         ForemanLoggedInNumber = xml.read(ConsumerTag.ForemanLoggedInNumber, FILEPATH);												
																									
	public static class ConsumerTag {

		public static final String MechanicLoggedInNumber = "MechanicLoggedInNumber",
				                                    ForemanLoggedInNumber = "ForemanLoggedInNumber";
	}



}
