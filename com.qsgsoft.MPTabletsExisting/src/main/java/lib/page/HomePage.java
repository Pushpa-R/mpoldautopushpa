package lib.page;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.w3c.css.sac.Locator;

import io.appium.java_client.MobileElement;
import io.appium.java_client.ios.IOSDriver;
import lib.locators.CreateExpense.Locators;

import qaframework.Configuration.Config_MobileAndWeb;
import qaframework.WebElement.WebTimeConstant;
import lib.locators.CreateFuel.FuelLocators;
import lib.locators.CreateJobHazard.JobHazardLocators;
import lib.locators.CreateIncident.IncidentLocators;
import lib.locators.CreateRoadReady.RoadReadyLocators;
import lib.locators.CreateProjectSite.ProjectSiteLocators;
import lib.locators.CreateMonthlyBluesheet.MonthlyBluesheetLocators;


public class HomePage {

	
	IOSDriver<MobileElement> driver;

	public HomePage(IOSDriver<MobileElement> _driver) throws Exception {
		super();
		this.driver = _driver;
	}

	public HomePage clickExpense() throws Exception {

		WebDriverWait wait = new WebDriverWait(driver,
				WebTimeConstant.WAIT_TIME_SMALL);

		 wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(Locators.expenseIcon)));
         driver.findElement(By.name(Locators.expenseIcon)).click();
		 return this;
	}
	
	public HomePage clickFuel() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, WebTimeConstant.WAIT_TIME_SMALL);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(FuelLocators.fuelIcon)));
		driver.findElement(By.name(FuelLocators.fuelIcon)).click();
		return this;
	}
	
	public HomePage clickJobHazard() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, WebTimeConstant.WAIT_TIME_SMALL);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(JobHazardLocators.jobHazardIcon)));
		driver.findElement(By.name(JobHazardLocators.jobHazardIcon)).click();
		return this;
	}
	
	public HomePage clickIncident() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, WebTimeConstant.WAIT_TIME_SMALL);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(IncidentLocators.incidentIcn)));
		driver.findElement(By.name(IncidentLocators.incidentIcn)).click();
		return this;
	}

	public HomePage clickRoadReady() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, WebTimeConstant.WAIT_TIME_SMALL);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(RoadReadyLocators.roadReadyIcon)));
		driver.findElement(By.name(RoadReadyLocators.roadReadyIcon)).click();
		return this;
	}
	
	public HomePage clickProjectSite() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, WebTimeConstant.WAIT_TIME_SMALL);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(ProjectSiteLocators.projectSiteIcn)));
		driver.findElement(By.name(ProjectSiteLocators.projectSiteIcn)).click();
		return this;
	}
	
	public HomePage clickMonthlyBluesheet() throws Exception {
		WebDriverWait wait = new WebDriverWait(driver, WebTimeConstant.WAIT_TIME_SMALL);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(MonthlyBluesheetLocators.monthlyBluesheetIcn)));
		driver.findElement(By.name(MonthlyBluesheetLocators.monthlyBluesheetIcn)).click();
		return this;	
	}
	
	/**********************************************************************************
	 * Description : This function used to tap on OK Button after Submission :
	 * 27-Aprill-2017 Author : Ashwini
	 **********************************************************************************/
	public HomePage okButtonTappedForAlert(String msg) throws Exception {
		Boolean iselementpresent = driver.findElements(By.name(Locators.OK)).size()!= 0;
	    if (iselementpresent == true)
	    {
		    System.out.print("Is Present On The Page");
		    WebDriverWait wait = new WebDriverWait(driver,
					WebTimeConstant.WAIT_TIME_SMALL);	
		    wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(Locators.OK)));
		    driver.findElement(By.name(Locators.OK)).click();    
		 }
		 else
		 {
		    System.out.print("is Not Present On The Page");
		 }		 
		return this;
	}
	
	/**********************************************************************************
	 * Description : This function used to tap on OK Button after Submission :
	 * 27-Aprill-2017 Author : Ashwini
	 **********************************************************************************/
	public HomePage allowButtonTapped(String msg) throws Exception {
		Boolean iselementpresent = driver.findElements(By.name(Locators.Allow)).size()!= 0;
	    if (iselementpresent == true)
	    {
		    System.out.print("Is Present On The Page");
		    WebDriverWait wait = new WebDriverWait(driver,
					WebTimeConstant.WAIT_TIME_SMALL);	
		    wait.until(ExpectedConditions.visibilityOfElementLocated(By.name(Locators.Allow)));
		    driver.findElement(By.name(Locators.Allow)).click();    
		 }
		 else
		 {
		    System.out.print("is Not Present On The Page");
		 }		 
		return this;
	}

}
