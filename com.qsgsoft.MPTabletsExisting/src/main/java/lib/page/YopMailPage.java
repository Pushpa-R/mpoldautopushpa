package lib.page;



import static org.testng.Assert.assertEquals;

import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.AssertJUnit;

import lib.locators.Yopmail.Locators;
import lib.locators.Yopmail.Locators.locators;
import qaframework.custom.WaitForElement;
import qaframework.PageObject.PageObject;
import qaframework.WebElement.WebTimeConstant;

public class YopMailPage extends PageObject {

	WebDriver driver;
	WaitForElement wait;

	
	public WebDriverWait explicitWait;
	public WaitForElement waitForElement;

	public YopMailPage(WebDriver _driver) throws Exception {
		super(_driver);
		this.driver = _driver;
		wait = new WaitForElement(this.driver);
	}
	
	/**********************************************************************************
	 * Description : This function launches yopmail.com website 
	 * Date: 08-May-2017
	 * Author : Pushpa
	 **********************************************************************************/
	public YopMailPage launchYopmail() throws Exception {
		driver.get(locators.urlOfYopmail);
		return this;
	}
	
	/**********************************************************************************
	 * Description : This function enter email of the agent and click to login
	 * see email 
	  * Date: 08-May-2017
	 * Author : Pushpa
	 **********************************************************************************/
	public YopMailPage enterAgentEmailAddressInYopmail(String aEmail) throws Exception {
		driver.findElement(By.id(locators.enterEmailAddress)).click();
		driver.findElement(By.id(locators.enterEmailAddress)).clear();
		driver.findElement(By.id(locators.enterEmailAddress)).sendKeys(aEmail);
		return this;
		
	}

	
	/*****************************************************************************
	 * Description	: This function is used to verify email ID field is present
	 * Arguments	: None
	  * Date: 08-May-2017
	 * Author : Pushpa
	 *****************************************************************************/	
	public YopMailPage verifyEmailIdField() throws Exception {
		
		AssertJUnit.assertTrue("email ID field is not present",
				this.page.element(locators.enterEmailAddress, "id").getOne().isDisplayed());		
		return this;
	}
	
	/*******************************************************************************
	 * Description	: This function is used to verify Check Inbox button is present
	 * Arguments	: None
	 * Date: 08-May-2017
	 * Author : Pushpa
	 *******************************************************************************/	
	public YopMailPage verifyCheckInboxButton() throws Exception {
		
		WebDriverWait wait = new WebDriverWait(this.driver,
				WebTimeConstant.WAIT_TIME_SMALL);
		
		wait.until(ExpectedConditions.presenceOfElementLocated(By
				.cssSelector(locators.click)));
		AssertJUnit.assertTrue("Check Inbox button is not present" ,
				this.page.element(locators.click, "css").getOne().isDisplayed());
		return this;
	}
	
	/*******************************************************************************
	 * Description	: This function is used to verify Check Inbox button is present
	 * Arguments	: None
	 * Date: 08-May-2017
	 * Author : Pushpa
	 *******************************************************************************/	
	public YopMailPage clickCheckInboxButton() throws Exception {
		       this.page.element(locators.click, "css").getOne().isDisplayed();
				this.page.element(locators.click, "css").getOne().click();
				Thread.sleep(10000);
		return this;
	}
	
	/********************************************************************************
	 * Description	: This function is used to click on 'Check For New mails' button
	 * Arguments	: None
	 * Date: 08-May-2017
	 * Author : Pushpa
	 ********************************************************************************/	
	public YopMailPage clickCheckForNewMailsButton() throws Exception {
		WebDriverWait wait = new WebDriverWait(this.driver,
				WebTimeConstant.WAIT_TIME_TOO_LONG);
		
		wait.until(ExpectedConditions.elementToBeClickable(By
				.cssSelector(locators.checkForNewMailsButton)));
		this.page.element(locators.checkForNewMailsButton, "css").getOne().isDisplayed();
		this.page.element(locators.checkForNewMailsButton, "css").getOne().click();	
		Thread.sleep(1000);
		return this;
	}
	
	/**********************************************************************************
	 * Description : This function used to verify subject in yopmail. 
	 *  Date: 26-12-2017
	 * Author : Pushpa
	 **********************************************************************************/
	public YopMailPage verifySubject() throws Exception {
		
		driver.switchTo().frame("ifmail");
		WebDriverWait wait = new WebDriverWait(this.driver,
				WebTimeConstant.WAIT_TIME_SMALL);
		
		wait.until(ExpectedConditions.presenceOfElementLocated(By
				.xpath(locators.verifySubject)));
		driver.findElement(By.xpath(locators.verifySubject)).isDisplayed();
		String Subject = driver.findElement(By.xpath(locators.verifySubject)).getText();
		System.out.println(Subject);
		return this;
			}
	
	/**********************************************************************************
	 * Description : This function used to click Attachment in yopmail. 
	 *  Date: 26-12-2017
	 * Author : Pushpa
	 **********************************************************************************/
	public YopMailPage clickAttachment(String [] msg) throws Exception {
		
		WebDriverWait wait = new WebDriverWait(this.driver,
				WebTimeConstant.WAIT_TIME_SMALL);
		//System.out.println("Actual" + msg);
		wait.until(ExpectedConditions.presenceOfElementLocated(By
				.xpath(locators.clickAttachment)));
		driver.findElement(By.xpath(locators.clickAttachment)).isDisplayed();
		 driver.findElement(By.xpath(locators.clickAttachment)).click();
		 
		 Path pdf = Paths.get("/Users/ravi/Downloads"+"//"+"job");
		 PDDocument pd = PDDocument.load(pdf.toFile());
		 PDFTextStripper stripper = new PDFTextStripper();
		 //save pdf text into strong variable
		 String strPDFtxt = stripper.getText(pd);
		 System.out.println("Expected" + strPDFtxt);
		// Assert.assertEquals(msg, strPDFtxt);
		
		return this;
			}


	/*******************************************************************************
	 * Description : This function is used to verify chat log text. Arguments :
	 * None Date: 08-May-2017 Author : Pushpa
	 *******************************************************************************/
	public YopMailPage verifyText() throws Exception {
		
		driver.switchTo().frame("ifmail");
		
		this.page.element(locators.chatLogAttachment, "xpath").getOne().isDisplayed();
		
	    String filename =this.page.element(locators.chatLogAttachment, "xpath").getOne().getText();
	    
	    System.out.println(filename);
	    
	    
		
		
	/*	this.page.element(locators.chatLogAttachment, "xpath").getOne().click();
		
		wait.waitForOneSecond();

		  File file = new File("C:\\Users\\Madhu\\Downloads\\", filename);
		   FileInputStream fis = new FileInputStream(file);
		   byte[] data = new byte[(int) file.length()];
		   fis.read(data);
		   fis.close();

		   String str = new String(data, "UTF-8");
		   System.out.println(str);
		   assertTrue(str.contains("stream"));*/

		this.page.element(locators.verifyEnableChatText, "xpath").getOne()
				.getText();
		return this;
	}

}
