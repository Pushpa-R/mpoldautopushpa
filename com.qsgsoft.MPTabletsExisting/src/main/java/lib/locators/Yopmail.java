package lib.locators;

public class Yopmail {
	
	public static class Locators {
	
			public class locators {

				public static final String urlOfYopmail = "http://www.yopmail.com",
						enterEmailAddress = "login",
						click = "input.sbut",
						checkForNewMailsButton = "span.slientext",
						verifyEnableChatText = "//div[@id='mailmillieu']/div[2]/div",
						chatLogAttachment = "//div[@id='mailhaut']/a",
						verifySubject = "//div[@id='mailmillieu']/div[2]/p",
						clickAttachment = "//div[@id='mailhaut']/a";

			}
			
	}

}
