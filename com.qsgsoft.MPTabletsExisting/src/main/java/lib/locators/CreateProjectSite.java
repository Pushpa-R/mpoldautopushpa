package lib.locators;

public class CreateProjectSite {
	public static class ProjectSiteLocators {
		public static final String projectSiteIcn = "projectSiteIcn",
				                                    addIcon = "plus 1",
				                                    date = "date",
				                                    foreman = "foreman",
				                                    jobNumber = "jobNumber",
				                                    crewLocation = "crewLocation",
				                                    siteInspection = "siteInspection",
				                                    corrNeededYes = "corrYes",
				                                    correctionText = "correctionTW",
				                                    workPracticeText = "workPraTW",
				                                    unsafeWorkNo = "unsafeNo",
				                                    unsafeWorkText = "unsafeCorrTW",
				                                    toolsYes = "toolYes",
				                                    toolsText = "toolTW",
				                                    ppeYes = "ppeNo",
				                                    ppeText = "ppeCorrTW",
				                                    attachButton = "Attach Pictures",
				                                    photoButton = "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeScrollView[1]/XCUIElementTypeOther[19]/XCUIElementTypeButton[1]",
				                                    cameraButton = "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeScrollView[1]/XCUIElementTypeOther[19]/XCUIElementTypeButton[2]",
				                                    inspectorSign = "inspectorSign",
				                                    inspectedName = "inspectedTF",
				                                    cameraRoll = "Camera Roll",
				                                    saveButton = "Save",
				                                    submitButton = "Submit",
				                                    alertForSubmit = "Your Project Site Audit Report is Submitted",
				                                    photoNameiOS11 = "Photo, Portrait, 26 April, 6:20 PM",
			    					                    photoNameiOS10 =	"Photo, Portrait, 23 April, 5:33 PM",				                                   
				                                    scrollView = "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]",
				                                    OK = "OK";		
		
		
	}

}
