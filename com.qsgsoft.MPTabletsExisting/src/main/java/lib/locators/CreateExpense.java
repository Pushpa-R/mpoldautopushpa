package lib.locators;

public class CreateExpense {
	
	public static class Locators {
		
		
	public static final String expenseIcon = "ExpenseBtn",
			                                    addIcon = "plus 1",
			                                    receiptDate = "ReceiptDateTF",
			                                    	employeeName = "EmployeeName",
			                                    	jobNumber = "JobNumber",
			                                    	vehicleNumber = "VehicleNumberTF",
			                                    	totalAmount = "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeScrollView[1]/XCUIElementTypeTextField[6]",
			                                    	//description = "Description",
			                                    description = 	"//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeScrollView[1]/XCUIElementTypeTextView[1]",
			                                    	//photoBtn = "PhotosBtn",
			                                    	photoBtn ="//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeScrollView[1]/XCUIElementTypeOther[9]/XCUIElementTypeButton[1]",
			                                    	//cameraBtn = "CameraBtn",
			                                    cameraBtn = "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeScrollView[1]/XCUIElementTypeOther[9]/XCUIElementTypeButton[2]",
			                                    	save = "Save",
			                                    	typeOfExpenseDropDown = "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeTable[1]/XCUIElementTypeCell[1]",
			                                    	typeOfExpense = "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeScrollView[1]/XCUIElementTypeButton[1]",
			                                    	date = "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[5]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeDatePicker[1]/XCUIElementTypeOther[1]/XCUIElementTypePickerWheel[1]",
	                                            month = "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[5]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeDatePicker[1]/XCUIElementTypeOther[1]/XCUIElementTypePickerWheel[2]",
	                                            year = "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[5]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeDatePicker[1]/XCUIElementTypeOther[1]/XCUIElementTypePickerWheel[3]",		
                                                submit = "Submit",
                                                OK = "OK",                                                
                                                Allow = "Allow",
                                                photoNameiOS11 = "Photo, Portrait, 26 April, 6:20 PM",
    					                            photoNameiOS10 =	"Photo, Portrait, 23 April, 5:33 PM",
    					                            cameraRoll = "Camera Roll",		
    					                            //alertForPhoto = ""void MPTablets(s)" Would Like to Access Your Photos",
    					                           // alertForCamera = ""MPTablets(s)" Would Like to Access Your Camera",
                                            	   alertSubmittedReport = "Your Expense Report is Submitted";
	}
	

}
