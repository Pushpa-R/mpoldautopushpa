package lib.locators;

public class CreateMonthlyBluesheet {
	public static class MonthlyBluesheetLocators {
		public static final String monthlyBluesheetIcn = "monthlyIcn",
				                                     addIcon = "plus 1",
				                                     date = "date",
				                                     unit = "unit",
				                                     odometer = "job",
				                                     job = "inspectedBy",
				                                     inspectedBy = "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeScrollView[1]/XCUIElementTypeTextField[5]",
				                                     firstAidKit = "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeScrollView[1]/XCUIElementTypeTable[1]/XCUIElementTypeCell[2]",
				                                     triangles = "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeScrollView[1]/XCUIElementTypeTable[1]/XCUIElementTypeCell[4]",
				                                     firstAidRadioYes = "radioYes",
				                                     fullComment = "comment",
				                                     trianglesGoodNo = "radioNo",
				                                     trianglesGoodComment = "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeScrollView[1]/XCUIElementTypeTable[2]/XCUIElementTypeCell[1]/XCUIElementTypeTextView[1]",
				                                     needMoreRadio = "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeScrollView[1]/XCUIElementTypeTable[2]/XCUIElementTypeCell[2]/XCUIElementTypeImage[3]",
				                                     needMoreComment = "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeScrollView[1]/XCUIElementTypeTable[2]/XCUIElementTypeCell[2]/XCUIElementTypeTextView[1]",
				                                     miscComment = "miscComment",
				                                     attachPhoto = "attachPhoto",
				                                     photoButton = "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeScrollView[1]/XCUIElementTypeButton[2]",
				                                     cameraButton = "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]/XCUIElementTypeScrollView[1]/XCUIElementTypeButton[3]",
				                                     OK ="OK",	
				                                     alertOkButton = "Ok",
				                                    	alertForSubmittedReport = "Your Monthly Safety Checklist Report is Submitted",
				                                     saveButton = "Save",
				                                     submit = "submit",
		                                             cameraRoll = "Camera Roll",	
		                                             scrollView = "//XCUIElementTypeApplication[1]/XCUIElementTypeWindow[2]/XCUIElementTypeOther[1]/XCUIElementTypeOther[1]",
		                                             photoNameiOS11 = "Photo, Portrait, 26 April, 6:20 PM",
                                                     photoNameiOS10 =	"Photo, Portrait, 23 April, 5:33 PM";			                                     				                                     
		}
}
