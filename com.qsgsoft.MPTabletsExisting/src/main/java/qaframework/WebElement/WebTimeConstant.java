package qaframework.WebElement;

public class WebTimeConstant {

	// Maximum and minimum wait time
	public static final int IMPLICIT_WAIT_TIME = 15, ELE_WAIT_TIME = 5,
			WAIT_TIME_SMALL = 10, WAIT_TIME_LONG = 120,
			WAIT_TIME_TOO_SMALL = 10, COUNT = 30, WAIT_TIME = 40,
			LOGIN_COUNT = 60, WAIT_TIME_ForLoading = 7, WAIT_TIME_False = 2,
			WAIT_TIME_TOO_LONG = 520;

}

